import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import classNames from 'classnames';

import { fetchUserAction, fetchUserSuccessAction, fetchUserErrorAction, patchUserNameAction, patchUserPreferencesAction } from './../../actions/userActions';

const loginUsers = [
	{
		name: 'admin',
		pass: 'admin',
	},
	{
		name: 'lucas',
		pass: 'gabardo',
	}
]

export class Login extends Component {
	constructor(props) {
    super(props)
    this.state = {
    	activeProps: [],
    }
    this.inputUser = React.createRef()
    this.inputPassword = React.createRef()
  }

  componentWillMount()  {
  	const { preferences } = this.props
  	this.setState({ activeProps: preferences })
  }

  componentWillReceiveProps()  {
  	const { preferences } = this.props
  	//this.setState({ activeProps: preferences })
  }

	handleSubmit = (e) => {
		e.preventDefault();
		const { fetchUserAction, fetchUserSuccessAction, fetchUserErrorAction, patchUserNameAction } = this.props
		const userValue = this.inputUser.current.value
		const passValue = this.inputPassword.current.value
		
		fetchUserAction()

		setTimeout(() => {
			const credetialOk = loginUsers.filter((user) => user.name === userValue && user.pass === passValue)

			if (credetialOk.length) {
				patchUserNameAction(credetialOk[0].name)
				fetchUserSuccessAction()
			} else {
				fetchUserErrorAction()
			}
		}, 500)
	}

	handleActive = (e, option) => {
		e.preventDefault();
		let newActives = []
		const { activeProps } = this.state
		if (activeProps.indexOf(option) > -1) {
			const index = activeProps.indexOf(option);
			newActives = [ ...activeProps ]
			if (index > -1) {
			  newActives.splice(index, 1);
			}
		} else {
			newActives = [ ...activeProps, option ]
		}
		this.setState({ activeProps: newActives })
	}

	handleSave = (e) => {
		e.preventDefault();
		const { patchUserPreferencesAction } = this.props
		const { activeProps } = this.state
		patchUserPreferencesAction(activeProps)
	}

	render () {
		const { isLogged, isLoading, error, userName, preferences } = this.props
		const { activeProps } = this.state
		console.log('render activeProps', activeProps)
		if (!isLogged) {
			return (
				<div className="login animation--fade">
					<h1 className="login__title">User Area</h1>
        			<form onSubmit={(e) => this.handleSubmit(e)}>
						<div className="form-group">
							<label className="label--1">Username</label>
							<input className="input-text--1" type="text" id="user" placeholder="Type your username" ref={this.inputUser} />
						</div>
						<div className="form-group">
							<label className="label--1">Password</label>
							<input className="input-text--1" type="password" id="password" placeholder="Type your password" ref={this.inputPassword} />
						</div>
						<div>
							<button className="button--1" type="submit">Login</button>
						</div>
					</form>
					{isLoading && 
						<div className="login__validating">Validating...</div> 
					}
					{error &&
						<div className="login__error">Invalid username or password, try again.</div>
					}
				</div>
			)
		} else {
			return (
				<div className="animation--fade">
    			<h1 className="login__title">Welcome, {userName}<span></span></h1>

    			<div className="interests">
    				<h2 className="login__title--2">My Interests</h2>
    				<nav className="interests__list">
    					<a onClick={(e) => this.handleActive(e, 'politics')} className={classNames('interests__list-item politics', { active: activeProps.indexOf('politics') > -1 })}>Politics</a>
    					<a onClick={(e) => this.handleActive(e, 'business')} className={classNames('interests__list-item business', { active: activeProps.indexOf('business') > -1 })}>Business</a>
    					<a onClick={(e) => this.handleActive(e, 'tech')} className={classNames('interests__list-item tech', { active: activeProps.indexOf('tech') > -1 })}>Tech</a>
    					<a onClick={(e) => this.handleActive(e, 'science')} className={classNames('interests__list-item science', { active: activeProps.indexOf('science') > -1 })}>Science</a>
    					<a onClick={(e) => this.handleActive(e, 'sports')} className={classNames('interests__list-item sports', { active: activeProps.indexOf('sports') > -1 })}>Sports</a>
    				</nav>

    				<button onClick={(e) => this.handleSave(e)} className="button--1" type="submit">SAVE</button>
    				<div>
    					<Link className="login__back" to="/">Back to Home</Link>
    				</div>
    			</div>
    		</div>
			)
		}
	}
}

const mapStateToProps = store => ({
  isLogged: store.userReducer.isLogged,
  isLoading: store.userReducer.isLoading,
  error: store.userReducer.error,
  userName: store.userReducer.userName,
  preferences: store.userReducer.preferences,
});

const mapDispatchToProps = { fetchUserAction, fetchUserSuccessAction, fetchUserErrorAction, patchUserNameAction, patchUserPreferencesAction };

export default connect(mapStateToProps, mapDispatchToProps)(Login);