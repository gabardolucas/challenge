import React, { Component } from 'react';
import { Route } from 'react-router-dom';

import Posts from "../../components/Posts/Posts";
import Login from "../../components/Login/Login";


export class Container extends Component {
	render () {
		return(
			<div className="container">
				<Route path="/" component={() => <Posts /> } exact />
				<Route path="/politics" component={() => <Posts filter="politics" /> } exact />
				<Route path="/business" component={() => <Posts filter="business" /> } exact />
				<Route path="/tech" component={() => <Posts filter="tech" /> } exact />
				<Route path="/science" component={() => <Posts filter="science" /> } exact />
				<Route path="/sports" component={() => <Posts filter="sports" /> } exact />
				<Route path="/login" component={() => <Login /> } exact />
			</div>
		)
	}
}

export default Container;