import React, { Component } from 'react';
import { Link } from 'react-router-dom';

export class Header extends Component {
	constructor(props) {
	    super(props);
	    this.state = {menuOpen: false};
	    this.handleClick = this.handleClick.bind(this);
  }

	handleClick() {
    this.setState(prevState => ({
      menuOpen: !prevState.menuOpen
    }));
  }

	render () {
		return(			
			<div className="header">			
				<div className="container">
					<a className="header__menu-icon visible-xs visible-sm" onClick={this.handleClick}><img src={require('./menu-icon.png')} alt="Menu"/></a>					

					<Link to="/" className="header__logo-wrapper">
						<img src={require('./logo.png')} alt="Logo" className="header__logo"/>		
					</Link>

					<nav className="header__nav hidden-xs hidden-sm">
						<Link className="header__nav-item" to="/">Home</Link>
						<Link className="header__nav-item" to="/politics">Politics</Link>
						<Link className="header__nav-item" to="/business">Business</Link>
						<Link className="header__nav-item" to="/tech">Tech</Link>
						<Link className="header__nav-item" to="/science">Science</Link>
						<Link className="header__nav-item" to="/sports">Sports</Link>
						<Link className="header__nav-item" to="/login">Login</Link>
					</nav>
				</div>

				<div className={"menu " + (this.state.menuOpen ? 'open' : '')}>
					<nav className="menu__list">
						<Link className="menu__list-item" onClick={this.handleClick} to="/">Home</Link>
						<Link className="menu__list-item" onClick={this.handleClick} to="/politics">Politics</Link>
						<Link className="menu__list-item" onClick={this.handleClick} to="/business">Business</Link>
						<Link className="menu__list-item" onClick={this.handleClick} to="/tech">Tech</Link>
						<Link className="menu__list-item" onClick={this.handleClick} to="/science">Science</Link>
						<Link className="menu__list-item" onClick={this.handleClick} to="/sports">Sports</Link>
						<Link className="menu__list-item" onClick={this.handleClick} to="/login">Login</Link>
					</nav>	
				</div>
			</div>
		)
	}
}

export default Header;