import { GET_POSTS_ACTION, GET_POSTS_SUCCESS_ACTION } from '../actions/actionTypes';

const initialState = {
  isLoading: false,
  posts: [],
};

export const postsReducer = (state = initialState, action) => {
  switch (action.type) {
    case GET_POSTS_ACTION:
      return {
        ...state,
        isLoading: true,
      };
    case GET_POSTS_SUCCESS_ACTION:
      return {
        ...state,
        isLoading: false,
        posts: action.posts,
      };
    default:
      return state;
  }
};