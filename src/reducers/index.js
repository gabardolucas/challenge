import { postsReducer } from './postsReducer';
import { userReducer } from './userReducer';
import { combineReducers } from 'redux';

export const Reducers = combineReducers({
  postsReducer: postsReducer,
  userReducer: userReducer,
});