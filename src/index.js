import React, { Component } from 'react';
import ReactDOM from "react-dom";
import Header from "./components/Header/Header";
import Container from "./components/Container/Container";
import { Provider } from 'react-redux';
import { Store } from './store';
import { BrowserRouter as Router } from 'react-router-dom';
import './styles/css/main.css';

class App extends Component {
	render () {
		return(
			<Router>
				<div>
						<Header />
						<Container />
				</div>
			</Router>
		)
	}
}

const rootElement = document.getElementById("root");
ReactDOM.render(<Provider store={Store}><App /></Provider>, rootElement);
