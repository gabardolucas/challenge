import { GET_POSTS_ACTION, GET_POSTS_SUCCESS_ACTION } from './actionTypes';

export const fetchPostsAction = () => ({
  type: GET_POSTS_ACTION
});

export const fetchPostsSuccessAction = posts => ({
  type: GET_POSTS_SUCCESS_ACTION,
  posts
});